/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Entidades.Usuario;
import java.util.List;
import javax.swing.JOptionPane;
import net.sourceforge.jwebunit.api.IElement;
import net.sourceforge.jwebunit.junit.WebTester;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author SEBAS
 */
public class NegAutomatizacion {

    private WebTester tester;

    public NegAutomatizacion() {
    }

    @Before
    public void SetURL() {
        tester = new WebTester();
        tester.setBaseUrl("http://sgt.tisal.cl");
    }

    @Test
    public void Login(Usuario U) throws InterruptedException {
        tester.beginAt("/Login.aspx"); 
        tester.setScriptingEnabled(true);
        tester.setTextField("TxtUsuario", U.getUsuario());
        tester.setTextField("TxtClave", U.getPassword());
       
        tester.clickElementByXPath("//input[@id='ingresar']");
        tester.gotoPage("http://sgt.tisal.cl/Rao/ingreso_horas.aspx");
        tester.getElementByXPath("//input[@id='txtNumeroHoras']").setAttribute("value", "8");
        Thread.sleep(300);
        tester.selectOptionByValue("cboCliente", "58");
        Thread.sleep(300);
        tester.selectOptionByValue("cboAmbito", "17");
        Thread.sleep(300);
        tester.selectOptionByValue("cboModulo", "2");
        Thread.sleep(300);
        tester.selectOptionByValue("cboTarea", "85");
        Thread.sleep(300);
        tester.getElementByXPath("//textarea[@name='observacion']").setAttribute("value", "PROYECTO BI");
        tester.setTextField("observacion", "Proyecto BI");
        Thread.sleep(300);
        tester.clickElementByXPath("//input[@id='btnIngresar']");
        Thread.sleep(300);
        tester.getElementByXPath("//button[@type='button']").setAttribute("value", "Si");
        tester.clickButtonWithText("Si");
        tester.clickButtonWithText("No");
        Thread.sleep(300);
    }

    @Test
    public int CantDias(Usuario U) throws InterruptedException {
        tester.beginAt("/Login.aspx"); 
        tester.setScriptingEnabled(true);
        tester.setTextField("TxtUsuario", U.getUsuario());
        tester.setTextField("TxtClave", U.getPassword());
        //  tester.submit();
        tester.clickElementByXPath("//input[@id='ingresar']");
        tester.gotoPage("http://sgt.tisal.cl/Rao/ingreso_horas.aspx");

        List<IElement> Fechas = tester.getElementsByXPath("//select[@id='cboFechaTrabajo']");
        int Dias = 0;

        for (IElement F : Fechas) {
            List<IElement> cb = F.getChildren();
            Dias = cb.size();
        }

        return Dias;
    }

    public void RAO(Usuario U) throws InterruptedException {

        int Dias = CantDias(U);
        JOptionPane.showMessageDialog(null, "Se ingresaran " + Dias + " como Proyecto BI");

        for (int i = 0; i < Dias; i++) {
            Login(U);
        }
        JOptionPane.showMessageDialog(null, "Horas Ingresadas!");

    }

}
